#!/bin/sh

add-apt-repository -y -n universe
add-apt-repository -y -n multiverse

add-apt-repository -y ppa:~ubuntu-unity-devs/stable

apt-get upgrade -y

apt-get install -y gsettings-ubuntu-schemas ubuntu-settings gnome-terminal dbus-x11 gnome-software-plugin-flatpak; apt-get purge -y gdm3 gnome-shell

#apt-get install -y unity unity-session notify-osd libnotify-bin unity-greeter lightdm \
#    unity-lens-applications unity-lens-files zeitgeist-datahub indicator-session \
#    indicator-power indicator-sound indicator-datetime indicator-application indicator-appmenu unity-control-center \
#    hud unity-tweak-tool yaru-unity7 ubuntu-unity-backgrounds ubuntu-unity-settings plymouth-theme-ubuntu-unity \
#    flatpak yaru-theme-unity atril eom pluma vlc

apt-get install -y ubuntu-unity-desktop ubuntu-unity-backgrounds ubuntu-unity-settings yaru-unity7 unity-tweak-tool plymouth-theme-ubuntu-unity flatpak yaru-theme-unity atril eom pluma vlc

apt-get install -y metacity --no-install-recommends

flatpak remote-add --system --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

apt-get purge -y gdm3 gnome-shell nautilus gnome-control-center apport-gtk eog totem evince gedit ubuntu-session --auto-remove

apt-get install -y metacity ubiquity ubiquity-slideshow-ubuntu ubiquity-frontend-gtk

apt-get purge -y budgie-core --auto-remove